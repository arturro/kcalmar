# Kcalmar


### Requirements
* python3.6
* sqlite3 / mysql

### Installation
- clone this repo:
```sh
$ git clone git@bitbucket.org:arturro/kcalmar.git
```

- create your virtualenv (if you prefer you can use venv) for this app.
    Go to your app directory, and type - on linux:
```sh
$ pip3 install virtualenv
$ python3 -m virtualenv venv
$ source ./venv/bin/activate
```
or
```sh
$ mkvirtualenv --python=/usr/bin/python3 venv
```

* install requirements:
```sh
$ cd kcalmar
$ pip install -r requirements-dev.txt
```

* Database:
    * SQLite - settints is ready for SQLite
    * MySQL - uncomment config in settings, and create db
```sh
CREATE DATABASE kcalmar CHARACTER SET utf8 COLLATE utf8_general_ci;
GRANT ALL PRIVILEGES ON kcalmar.* TO 'kcalmar'@'localhost' IDENTIFIED BY "kcalmar";

CREATE DATABASE test_kcalmar CHARACTER SET utf8 COLLATE utf8_general_ci;
GRANT ALL PRIVILEGES ON test_kcalmar.* TO 'kcalmar'@'localhost' IDENTIFIED BY "kcalmar";
```

* load data, run server:
```sh
$ cd django
$ python manage.py migrate
$ python manage.py loaddata ./core/fixtures/initial_data.json
$ python manage.py runserver
```

### Tests
for test run:
```sh
$ source ./venv/bin/activate
$ cd django;
$ python manage.py test
```

### Todos
