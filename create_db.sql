CREATE DATABASE kcalmar CHARACTER SET utf8 COLLATE utf8_general_ci;
GRANT ALL PRIVILEGES ON kcalmar.* TO 'kcalmar'@'localhost' IDENTIFIED BY "kcalmar";

CREATE DATABASE test_kcalmar CHARACTER SET utf8 COLLATE utf8_general_ci;
GRANT ALL PRIVILEGES ON test_kcalmar.* TO 'kcalmar'@'localhost' IDENTIFIED BY "kcalmar";
