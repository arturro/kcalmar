$(function () {
    // This function gets cookie with a given name
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    var csrftoken = getCookie('csrftoken');

    /*
    The functions below will create a header with csrftoken
    */

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    function sameOrigin(url) {
        // test that a given url is a same-origin URL
        // url could be relative or scheme relative or absolute
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        // Allow absolute or scheme relative URLs to same origin
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
            // or any other URL that isn't scheme relative or absolute i.e relative.
            !(/^(\/\/|http:|https:).*/.test(url));
    }

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                // Send the token to same-origin, relative URLs only.
                // Send the token only if the method warrants CSRF protection
                // Using the CSRFToken value acquired earlier
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

});

$(function () {
    $('#user-appointment-form').on('submit', function (event) {
        event.preventDefault();
        console.log("form submitted!"); // sanity check
        $('#error').html('');
        $('.error_field').remove();
        create_appointment();
    });

    function create_appointment() {
        $.ajax({
            url: $(location).attr('href'), // the endpoint
            type: "POST", // http method
            data: {
                name: $('#id_name').val(),
                email: $('#id_email').val(),
                dietician_id: $('#id_dietician_id').val(),
                day: $('#id_day').val(),
                hour: $('#id_hour').val()

            }, // data sent with the post request

            // handle a successful response
            success: function (json) {
                $('#post-text').val(''); // remove the value from the input
                console.log(json); // log the returned json to the console
                window.location.href = json.redirect;
            },

            error: function (xhr, errmsg, err) {
                var data = jQuery.parseJSON(xhr.responseText)
                if (data.error) {
                    $('#error').html('<div data-alert>' + data.error + '</div>');
                }
                $.each(data.errors, function (field, errors) {
                    html_errors = '<ul id="id_error_' + field + '" class="error_field">';
                    $.each(errors, function (index, error) {
                        html_errors += '<li>' + error + '</li>';
                    });
                    html_errors += '</ul>';
                    $('#id_' + field).parent().prepend(html_errors);
                    console.log(field + ": " + errors);
                });
                console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
            }
        });
    }
});


