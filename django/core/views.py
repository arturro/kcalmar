import logging
from pprint import pformat

from django.db import IntegrityError
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.urls import reverse
from django.views import generic

from .const import APPOINTMENT_INVALID_FORM, APPOINTMENT_TAKEN_MSG, \
    APPOINTMENT_UNKNOWN_ERROR
from .forms import AppointmentForm
from .models import Dietician, Appointment

logger = logging.getLogger('django')


class DieticianIndexView(generic.ListView):
    template_name = 'dietician/index.html'

    def get_queryset(self):
        return Dietician.objects.order_by('surname')[:10]


class DieticianChooseAppointmentView(generic.DetailView):
    model = Dietician
    template_name = 'dietician/choose_appointment.html'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        context['appointments'] = \
            Appointment.get_appointment_for_dietician(self.object)
        return self.render_to_response(context)


class DieticianPersonalDataView(generic.DetailView):
    model = Dietician
    template_name = 'dietician/personal_data.html'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        context = self.get_context_data(object=self.object)
        context['appointments'] = \
            Appointment.get_appointment_for_dietician(self.object)
        return self.render_to_response(context)


def dietician_personal_data(request, dietician_id, day, hour):
    is_ajax = request.is_ajax()
    context = {'error': ''}
    name = ''
    email = ''
    if request.method == 'POST':
        form = AppointmentForm(request.POST)
        if form.is_valid():
            dietician_id = form.cleaned_data['dietician_id']
            dietician = Dietician.objects.get(pk=dietician_id)
            data = {
                'dietician': dietician,
                'day': form.cleaned_data['day'],
                'hour': form.cleaned_data['hour'],
                'name': form.cleaned_data['name'],
                'email': form.cleaned_data['email'],
            }
            appointment = Appointment(**data)
            try:
                appointment.save()
                appointment.send_confirmation()
                if is_ajax:
                    return JsonResponse({
                        'status': True,
                        'redirect': reverse('core:thanks'),
                    })
                else:
                    return HttpResponseRedirect(reverse('core:thanks'))
            except IntegrityError:
                error = APPOINTMENT_TAKEN_MSG
                logger.exception(error)
            except Exception:
                error = APPOINTMENT_UNKNOWN_ERROR
                logger.exception(error)
            if is_ajax:
                return JsonResponse({
                    'status': False,
                    'error': error, },
                    status=400,
                )
            else:
                context['error'] = error
                name = form.cleaned_data['name']
                email = form.cleaned_data['email']
        else:
            if is_ajax:
                response = JsonResponse({
                    'status': False,
                    'error': APPOINTMENT_INVALID_FORM,
                    'errors': form.errors, },
                    status=400,
                )
                return response
            logger.error('invalid form')
            logger.debug(pformat(form.errors))
            context['form'] = form
        return render(request, 'dietician/personal_data.html', context)

    form = AppointmentForm()
    form.fields['dietician_id'].initial = dietician_id
    form.fields['day'].initial = day
    form.fields['hour'].initial = hour
    form.fields['name'].initial = name
    form.fields['email'].initial = email
    context['form'] = form
    dietician = Dietician.objects.get(pk=dietician_id)
    data = {
        'dietician': dietician,
        'day': day,
        'hour': hour,
    }
    appointment = Appointment(**data)
    context['day'] = appointment.get_day_display()
    context['hour'] = appointment.get_hour_display()
    context['dietician'] = Dietician.objects.get(pk=dietician_id)
    return render(request, 'dietician/personal_data.html', context)


def thanks(request):
    message = 'Dziekujemy za rezerwację.'
    return render(request, 'dietician/thanks.html', {'message': message})


class AppointmentList(generic.ListView):
    model = Appointment

    def get_queryset(self):
        return Appointment.objects.order_by(
            'day', 'hour', 'dietician__surname')[:10]
