from django.urls import path

from . import views

app_name = 'core'
urlpatterns = [
    path('', views.DieticianIndexView.as_view(), name='index'),
    path(
         'dietician/<int:pk>/',
         views.DieticianChooseAppointmentView.as_view(),
         name='dietician_detail'),
    path(
        'dietician/<int:dietician_id>/appointment/<int:day>/<int:hour>/',
        views.dietician_personal_data,
        name='dietician_personal_data'),
    path('thanks', views.thanks, name='thanks'),
    path(
        'appointments',
        views.AppointmentList.as_view(),
        name='appointment_list'),
]
