import factory
from . import models


class DieticianFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Dietician

    name = factory.Faker('first_name')
    surname = factory.Faker('last_name')


class AppointmentFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = models.Appointment

    dietician = factory.SubFactory(DieticianFactory)
    day = 1
    hour = 1000
    name = factory.Faker('first_name')
    email = 'jakis@test.pl'
