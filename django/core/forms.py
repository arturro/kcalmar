import logging

from django import forms

logger = logging.getLogger('django')


class AppointmentForm(forms.Form):
    name = forms.CharField(label='Your name', max_length=128)
    email = forms.EmailField(label='Your email')
    dietician_id = forms.IntegerField(widget=forms.widgets.HiddenInput())
    day = forms.IntegerField(widget=forms.widgets.HiddenInput())
    hour = forms.IntegerField(widget=forms.widgets.HiddenInput())


# class AppointmentOldForm(forms.ModelForm):
#     class Meta:
#         model = Appointment
#         fields = ['dietician', 'day', 'hour', 'name', 'email']
