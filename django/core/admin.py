from django.contrib import admin

from .models import Dietician, Appointment

admin.site.register(Dietician)
admin.site.register(Appointment)
