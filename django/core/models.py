import logging

from django.core.mail import send_mail
from django.db import models
from django.template.loader import get_template

logger = logging.getLogger('django')


class Dietician(models.Model):
    EMPTY = 0
    MR = 1
    MS = 2
    PREFIXES = (
        (EMPTY, ''),
        (MR, 'dietetyk'),
        (MS, 'dietetyczka'),
    )
    PREFIXES_DICT = {values[0]: values[1] for values in PREFIXES}

    prefix = models.IntegerField(choices=PREFIXES, default=EMPTY)
    name = models.CharField(max_length=128)
    surname = models.CharField(max_length=128)

    def __str__(self):
        return '{} {} {}'.format(
            self.get_prefix_display(), self.name, self.surname)


class Appointment(models.Model):
    DAYS = (
        (1, 'pon'),
        (2, 'wt'),
        (3, 'sr'),
        (4, 'cz'),
        (5, 'pt'),
        (6, 'sb'),
        (7, 'nd'),
    )
    HOURS = []
    for hour in range(0, 24):
        for minute in (0, 30):
            HOURS.append((
                int('{:02d}{:02d}'.format(hour, minute)),
                '{:02d}:{:02d}'.format(hour, minute),))
    CONFIRMATION_SUBJECT = "Potwierdzenie wizyty w systemie"

    dietician = models.ForeignKey(Dietician, on_delete=models.CASCADE)
    day = models.IntegerField(choices=DAYS)
    hour = models.IntegerField(choices=HOURS)
    name = models.CharField(max_length=128)
    email = models.EmailField()

    def send_confirmation(self):
        sender = 'from@example.com'
        recipients = [self.email]
        data = {
            'name': self.name,
            'day': self.get_day_display(),
            'hour': self.get_hour_display(),
            'dietician': self.dietician,
        }
        plaintext = get_template('email/appointment_confirmation.txt')
        text_content = plaintext.render(data)
        send_mail(self.CONFIRMATION_SUBJECT, text_content, sender, recipients)

    @classmethod
    def get_appointment_for_dietician(cls, dietician):
        appointment = {}
        for day, day_name in cls.DAYS:
            appointment[day] = {
                'day': day_name,
                'hours': {}
            }
            for hour, hour_name in cls.HOURS:
                appointment_hour = {
                    'hour': hour_name,
                    'available': True
                }
                appointment[day]['hours'][hour] = appointment_hour
        taken_appointments = cls.objects.filter(dietician=dietician)
        for taken_appointment in taken_appointments:
            appointment[
                taken_appointment.day]['hours'][taken_appointment.hour][
                'available'] = False
        return appointment

    class Meta:
        unique_together = ('dietician', 'day', 'hour')

    def __str__(self):
        data = {
            'day': self.get_day_display(),
            'hour': self.get_hour_display(),
            'name': self.name,
            'email': self.email,
        }
        return '{day} - {hour} {name} <{email}>'.format(**data)
