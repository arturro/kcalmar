from django.core import mail
from django.db.utils import IntegrityError
from django.test import TestCase
from django.urls import reverse

from .factories import DieticianFactory, AppointmentFactory
from .models import Appointment


class AppointmentModelTestCase(TestCase):
    def setUp(self):
        self.d1 = DieticianFactory()
        self.d2 = DieticianFactory()

    def test_the_same_date_should_throw_error(self):
        with self.assertRaises(IntegrityError):
            AppointmentFactory(dietician=self.d1, day=1, hour=1000)
            AppointmentFactory(dietician=self.d1, day=1, hour=1000)

    def test_the_diffrent_date_should_return_ok(self):
        AppointmentFactory(dietician=self.d1, day=1, hour=1000)
        AppointmentFactory(dietician=self.d1, day=1, hour=1030)

    def test_the_diffrent_dietician_should_return_ok(self):
        AppointmentFactory(dietician=self.d1, day=1, hour=1000)
        AppointmentFactory(dietician=self.d2, day=1, hour=1030)


class AppointmentViewTestCase(TestCase):
    def setUp(self):
        self.d1 = DieticianFactory()
        self.d2 = DieticianFactory()
        self.appointment_data = {
            'dietician_id': self.d1.pk,
            'day': 1,
            'hour': 1000,
            'name': 'artur',
            'email': 'artur@test.pl'
        }
        self.kwargs_url_step3 = {
            'dietician_id': self.appointment_data['dietician_id'],
            'day': self.appointment_data['day'],
            'hour': self.appointment_data['hour'],
        }
        self.url_step3 = reverse(
            'core:dietician_personal_data', kwargs=self.kwargs_url_step3)
        self.url_step4 = reverse('core:thanks')

    def test_post_correct_data(self):
        response = self.client.post(
            self.url_step3, data=self.appointment_data, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertRedirects(response, self.url_step4, 302, 200)
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(
            mail.outbox[0].subject, Appointment.CONFIRMATION_SUBJECT)

    def test_post_incorrect_data(self):
        self.appointment_data['email'] = 'invalid'
        response = self.client.post(
            self.url_step3, data=self.appointment_data, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(mail.outbox), 0)
